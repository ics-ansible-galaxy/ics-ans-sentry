ics-ans-sentry
===================

Ansible playbook to install Sentry.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
